include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'

variables:
  # REGISTRY: buildpkg
  REGISTRY: registry.gitlab.com/orange-opensource/gitlab-buildpkg
  BUILD_IMAGE: debian:bookworm # use a build image that supports zstd format also.
  BUILD_UPDATE: 'false'
  DEPLOY_MANUAL: 'false'

stages:
  - build
  - sign
  - deploy
  - staging
  - production

#
# Common rule predicates
#

.if-merge-request: &if-merge-request
  if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

.if-unprotected: &if-unprotected
  if: '$CI_COMMIT_REF_PROTECTED != "true"'

.if-build-manual: &if-build-manual
  if: '$BUILD_MANUAL == "true" || ( $BUILD_ONLY && $BUILD_ONLY != $BUILD_IMAGE )'

.if-deploy-info-missing: &if-deploy-info-missing
  if: '$DEPLOY_HOST == null || $DEPLOY_HOST == "" || $DEPLOY_USER == null || $DEPLOY_USER == ""'

.if-deploy-manual: &if-deploy-manual
  if: '$DEPLOY_MANUAL == "true"'

#
# Build
#

.build-base:
  image: $REGISTRY/$BUILD_IMAGE
  artifacts:
    expire_in: 1 day
    paths:
      - result/*

.build-deb:
  extends: .build-base
  script:
    - if [ "$BUILD_UPDATE" = "true" ] ; then apt-get update ; fi
    - ci-build-pkg

.build-rpm:
  extends: .build-base
  script:
    - if [ "$BUILD_UPDATE" = "true" ] ; then yum update ; fi
    - ci-build-pkg

build-debian:
  extends: .build-deb
  stage: build
  rules:
    - if: '$BUILD_IMAGE !~ /^debian:/'
      when: never
    - <<: *if-build-manual
      when: manual
      allow_failure: true
    - when: on_success

build-ubuntu:
  extends: .build-deb
  stage: build
  rules:
    - if: '$BUILD_IMAGE !~ /^ubuntu:/'
      when: never
    - <<: *if-build-manual
      when: manual
      allow_failure: true
    - when: on_success

build-centos:
  extends: .build-rpm
  stage: build
  rules:
    - if: '$BUILD_IMAGE !~ /^centos:/'
      when: never
    - <<: *if-build-manual
      when: manual
      allow_failure: true
    - when: on_success

build-rockylinux:
  extends: .build-rpm
  stage: build
  rules:
    - if: '$BUILD_IMAGE !~ /^rockylinux:/'
      when: never
    - <<: *if-build-manual
      when: manual
      allow_failure: true
    - when: on_success

build-fedora:
  extends: .build-rpm
  stage: build
  rules:
    - if: '$BUILD_IMAGE !~ /^fedora:/'
      when: never
    - <<: *if-build-manual
      when: manual
      allow_failure: true
    - when: on_success

#
# Sign
#

sign:
  image: $REGISTRY/$BUILD_IMAGE
  stage: sign
  variables:
    GIT_STRATEGY: none
  #    SIGN_USER: firstname.lastname@orange.com
  #    GPG_PRIVATE_KEY:
  script:
    - test -n "$GPG_PRIVATE_KEY"
    - cd $CI_PROJECT_DIR
    - ci-sign-pkg
  artifacts:
    expire_in: 1 day
    paths:
      - result/*
  rules:
    - <<: *if-merge-request
      when: never
    - <<: *if-unprotected
      when: never
    - when: on_success

#
#  Deploy
#

pages:
  image: $REGISTRY/$BUILD_IMAGE
  stage: deploy
  variables:
    PAGES_HOST: $CI_PAGES_DOMAIN
    GIT_STRATEGY: none
  environment:
    name: PPA
    url: $CI_PAGES_URL
  dependencies:
    - sign
  artifacts:
    paths:
      - public
  script:
    - cd "$CI_PROJECT_DIR"
    - ci-pages-ppa
    - ci-pages-home
    - ci-pages-tree
  rules:
    - <<: *if-merge-request
      when: never
    - <<: *if-unprotected
      when: never
    - <<: *if-deploy-manual
      when: manual
      allow_failure: true
    - when: on_success

.deploy-base:
  image: $REGISTRY/$BUILD_IMAGE
  variables:
    GIT_STRATEGY: none
  #    DEPLOY_MANUAL: 'false'
  #    DEPLOY_USER: usually a cuid
  #    DEPLOY_HOST: the target hostname or ip
  #    DEPLOY_DIR: the target deploy dir (optional)
  #    DEPLOY_UPDATE: the target deploy program (optional)
  #    SSH_PRIVATE_KEY: the public key is to be declared on the target
  dependencies:
    - sign
  script:
    - test -n "$DEPLOY_HOST"
    - test -n "$DEPLOY_USER"
    - test -n "$SSH_PRIVATE_KEY"
    - cd "$CI_PROJECT_DIR"
    - ci-deploy-pkg "${CI_ENVIRONMENT_NAME,,}"


#
# Staging
#

deploy-staging:
  extends: .deploy-base
  stage: staging
  environment:
    name: Staging
  rules:
    - <<: *if-merge-request
      when: never
    - <<: *if-unprotected
      when: never
    - <<: *if-deploy-info-missing
      when: never
    - <<: *if-deploy-manual
      when: manual
      allow_failure: true
    - when: on_success

#
# Production
#

deploy-production:
  extends: .deploy-base
  stage: production
  environment:
    name: Production
  rules:
    - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    - <<: *if-merge-request
      when: never
    - <<: *if-unprotected
      when: never
    - <<: *if-deploy-info-missing
      when: never
    - <<: *if-deploy-manual
      when: manual
      allow_failure: true
    - when: on_success

